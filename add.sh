#!/bin/bash

# Remove all old packages
reprepro -V removefilter community 'Section'

# Add new packages
reprepro --ignore=forbiddenchar -S main -P extra includedeb community ../debs-all/*.deb
